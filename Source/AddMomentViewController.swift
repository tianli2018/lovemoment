//
//  AddMomentViewController.swift
//  LoveMoment
//
//  Created by Zhang Tianli on 20/12/17.

//

import UIKit
import ImagePicker

class AddMomentViewController: UIViewController {

    @IBOutlet var txtView: UITextView!
    private var dataManager = DataManager.instance
    private var fileManager = FileManager.default
    fileprivate var imgUrl :String?
    
    @IBAction func onTapCancel(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func onTapCamera(_ sender: UIButton) {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true)
    }
    
    @IBAction func onTapOK(_ sender: UIButton) {
        dataManager.deleteDataFile()
        let current = dataManager.loveMoments!
        var new = LoveMoment()
        new.imgUrl = imgUrl
        new.text = txtView.text
        new.time = Date().format(DateFormat.pattern("yyyy-MM-dd HH:mm:ss"),humanFriendly: false)
        let moments = current + [new]
        dataManager.createFile(with: moments.toJSONString(prettyPrint: false)!)
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}

extension AddMomentViewController: ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
    
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count > 0 {
            let data = UIImageJPEGRepresentation(images[0], 0.025)
            let imgFileName = "\(Int64(Date().timeIntervalSince1970)).JPEG"
            let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(imgFileName)
            fileManager.createFile(atPath: url.path, contents: data)
            imgUrl = imgFileName
            dismiss(animated: true)
        }else{
            self.showAlert(message: "Photo selected is not valid, please select other photo!")
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        dismiss(animated: true)
    }
    
    
}
