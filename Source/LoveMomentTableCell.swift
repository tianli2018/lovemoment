//
//  LoveMomentTableCell.swift
//  sample
//
//  Created by Zhang Tianli on 15/11/17.

//

import UIKit
import SwipeCellKit

class LoveMomentTableCell: SwipeTableViewCell {
    
    @IBOutlet var loveImage: UIImageView?
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var txtContent: UITextView!
    
    var model: LoveMoment?{
        didSet{
            if let imgUrl = model?.imgUrl {
                let fileManager = FileManager.default
                let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(imgUrl)
                loveImage?.contentMode = .scaleAspectFill
                loveImage?.image = UIImage(contentsOfFile:url.path)
                
            }
            lblTime.text = model?.time!
            txtContent.text = model?.text!
        }
    }
    
}
