//
//  DataManager.swift
//  LoveMoment
//
//  Created by Zhang Tianli on 12/10/17.

//

import Foundation

class DataManager {
    private static let fileName = "myLove"
    private static let _instance = DataManager()
    private var fileManager = FileManager.default
    
    var loveMoments : [LoveMoment]? = [LoveMoment]()
    
    static var instance : DataManager {
        return _instance
    }
    
    func loadFromFile() -> Bool {
        let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(DataManager.fileName)
        
        if fileManager.fileExists(atPath: url.path) {
            do {
                let contents = try String(contentsOf: url, encoding : .utf8)
                loveMoments = [LoveMoment].deserialize(from: contents) as? [LoveMoment]
                if loveMoments == nil {
                    if let temp = (LoveMoment.deserialize(from: contents)){
                        loveMoments = [LoveMoment]() + [temp]
                        return true
                    }else{
                        return false
                    }
                }else {
                    return true
                }
                
            }
            catch {
                return false
            }
        }
        return false
    }
    
    func createFile(with json: String) {
        let data = json.data(using: .utf8)
        let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(DataManager.fileName)
        fileManager.createFile(atPath: url.path, contents: data)
        loveMoments = ([LoveMoment].deserialize(from: json) as? [LoveMoment])
        if loveMoments == nil {
            loveMoments = [LoveMoment]() + [(LoveMoment.deserialize(from: json))!]
        }
        
    }
    
    func deleteDataFile(){
        let paths = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(DataManager.fileName)
        do { try fileManager.removeItem(at: paths) } catch { }
    }
}

