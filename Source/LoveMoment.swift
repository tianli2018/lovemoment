//
//  LoveMoment.swift
//  LoveMoment
//
//  Created by Zhang Tianli on 5/12/17.

//

import Foundation
import HandyJSON

struct LoveMoment: HandyJSON {
    var time: String?
    var text: String?
    var imgUrl: String?
}

