//
//  LoveMomentViewController.swift
//  sample
//
//  Created by Zhang Tianli on 14/11/17.
//  Copyright © 2017 Tianli. All rights reserved.
//

import UIKit
import SwipeCellKit

class LoveMomentViewController: UIViewController {
    
    @IBOutlet weak var loveMomentTable: UITableView!
    var model: [LoveMoment]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = DataManager.instance.loveMoments
        loveMomentTable.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refresh()
    }
    
    func refresh() {
        model = DataManager.instance.loveMoments
        loveMomentTable.reloadData()
    }
    
    func deleteMoment(at index: Int) {
        let dataManager = DataManager.instance
        let removed = dataManager.loveMoments!.remove(at: index)
        dataManager.deleteDataFile()
        dataManager.createFile(with: dataManager.loveMoments!.toJSONString(prettyPrint: false)!)
        deletePic(moment: removed)
        refresh()
    }
    
    func deletePic(moment: LoveMoment) {
        if let imgUrl = moment.imgUrl {
            let fileManager = FileManager.default
            let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(imgUrl)
            if fileManager.fileExists(atPath: url.path) && fileManager.isDeletableFile(atPath: url.path) {
                do { try fileManager.removeItem(atPath: url.path) } catch {}
            }
        }
    }
}

extension LoveMomentViewController: UITableViewDelegate {
    
}

extension LoveMomentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "loveMomentTableCell", for: indexPath) as! LoveMomentTableCell
        cell.delegate = self
        if let model = model{
            cell.model = model[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 144
    }
}

extension LoveMomentViewController: SwipeTableViewCellDelegate{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            self.deleteMoment(at: indexPath.row)
        }
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete")
        
        return [deleteAction]
        
    }
    
    
}
