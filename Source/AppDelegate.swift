//
//  AppDelegate.swift
//  sample
//
//  Created by Zhang Tianli on 14/11/17.

//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        let dataManager = DataManager.instance
        if !dataManager.loadFromFile() {
            createWelcomeMsg(dataManager)
        }
        
        return true
    }
    
    fileprivate func createWelcomeMsg(_ dataManager: DataManager) {
        let fileManager = FileManager.default
        let welcomeImgData = UIImageJPEGRepresentation(UIImage.init(named: "welcome")!,0.025)
        let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("welcome").appendingPathExtension("JPEG")
        fileManager.createFile(atPath: url.path, contents: welcomeImgData)
        var welcome = LoveMoment()
        welcome.time = Date().format(DateFormat.pattern("yyyy-MM-dd HH:mm:ss"),humanFriendly: false)
        welcome.text = "Welcome to DT Moment!"
        welcome.imgUrl = "welcome.JPEG"
        
        let json = welcome.toJSONString(prettyPrint: false)
        dataManager.createFile(with: json!)
    }
}

