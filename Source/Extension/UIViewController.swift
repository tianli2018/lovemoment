

import UIKit

extension UIViewController {
    public func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc public func dismissKeyboard() {
        view.endEditing(true)
    }
    
    public func showAlert(title: String = "Love Moment", message: String, dismissButtonText: String = "OK", dismissHandler: @escaping (UIAlertAction) -> Void = { _ in
        }) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: dismissButtonText, style: UIAlertActionStyle.default, handler: dismissHandler))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func gotoPreviousView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}



